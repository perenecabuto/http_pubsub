package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "4000"
	}

	log.Println("Starting server at port:", port)
	log.Fatal(http.ListenAndServe("0.0.0.0:"+port, NewMessageHandler()))
}
