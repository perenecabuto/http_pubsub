package main

import (
	"errors"
	"log"
	"strconv"

	"github.com/oleiade/lane"
)

const (
	MAX_MESSAGES_PER_QUEUE = 10
	MAX_DEVICES            = 10
)

type Message []byte
type MessageQueue map[string]*lane.Queue

func (mq MessageQueue) Enqueue(uid string, data Message) error {
	queue, ok := mq[uid]
	if !ok {
		if len(mq) == MAX_DEVICES {
			return errors.New("Max devices (" + strconv.Itoa(MAX_DEVICES) + ") connection reached")
		}
		log.Println("Creating queue:", uid)
		queue = lane.NewQueue()
		mq[uid] = queue
	}

	for queue.Size() >= MAX_MESSAGES_PER_QUEUE {
		queue.Dequeue()
	}

	queue.Enqueue(data)
	return nil
}

func (mq MessageQueue) Dequeue(uid string) (Message, error) {
	queue, ok := mq[uid]
	if !ok {
		return nil, errors.New("Queue " + uid + " not found")
	}

	message := queue.Dequeue()
	if message == nil {
		return nil, errors.New("No message in queue " + uid)
	}
	return message.(Message), nil
}

func (mq MessageQueue) HasMessage(uid string) bool {
	queue, ok := mq[uid]
	return ok && queue.Head() != nil
}
