#!/bin/bash

UUID="$(uuid)"
ADDRESS="http://http-pubsub.herokuapp.com/"

for x in $(seq 0 20); do
    curl -X POST -H "DeviceID: $UUID" -H "Data: xxx-$x" $ADDRESS
done

for x in $(seq 0 11); do
    curl -I -H "DeviceID: $UUID" $ADDRESS
    read -p "Get next ($x)"
    curl -i -H "DeviceID: $UUID" $ADDRESS
done
