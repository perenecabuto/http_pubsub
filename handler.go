package main

import (
	"io"
	"log"
	"net/http"

	"code.google.com/p/go-uuid/uuid"
)

type MessageHandler struct {
	MessageQueue
}

func NewMessageHandler() *MessageHandler {
	mq := make(MessageQueue)
	return &MessageHandler{mq}
}

func (h MessageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	uid := r.Header.Get("DeviceID")
	if uuid.Parse(uid) == nil {
		errMsg := "DeviceID (" + uid + ") is invalid - try " + uuid.New()
		log.Println(errMsg)
		http.Error(w, errMsg, http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/http+pubsub")

	switch r.Method {
	case "HEAD":
		if !h.HasMessage(uid) {
			log.Println("No message for uuid:", uid)
			w.WriteHeader(http.StatusNotFound)
			return
		}

		log.Println("Has message for uuid:", uid)
		w.WriteHeader(http.StatusNoContent)
	case "GET":
		message, err := h.Dequeue(uid)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		log.Println("Dequeue data for uuid:", uid)
		io.WriteString(w, string(message))
	case "POST", "PUT":
		data := r.Header.Get("Data")
		if data == "" {
			errMsg := "Data is empty"
			log.Println(errMsg)
			http.Error(w, errMsg, http.StatusNotAcceptable)
			return
		}

		if err := h.Enqueue(uid, Message(data)); err != nil {
			log.Println(err.Error())
			http.Error(w, err.Error(), http.StatusServiceUnavailable)
			return
		}

		log.Println("Enqueued data for uuid:", uid)
	}
}
